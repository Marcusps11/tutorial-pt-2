module.exports = {
  apps: [{
    name: 'tutorial-2',
    script: './index.js'
  }],
  deploy: {
    production: {
      user: 'ubuntu',
      host: 'ec2-34-242-168-249.eu-west-1.compute.amazonaws.com',
      key: '~/.ssh/test.pem',
      ref: 'origin/master',
      repo: 'git@bitbucket.org:Marcusps11/tutorial-pt-2.git',
      path: '/home/ubuntu/tutorial-2',
      'post-deploy': 'npm install && pm2 startOrRestart ecosystem.config.js'
    }
  }
}